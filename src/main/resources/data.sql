
INSERT INTO world (name, system) VALUES ('Mercurio', 'Sistema Solare');
INSERT INTO world (name, system) VALUES ('Venere', 'Sistema Solare');
INSERT INTO world (name, system) VALUES ('Terra', 'Sistema Solare');
INSERT INTO world (name, system) VALUES ('Marte', 'Sistema Solare');
INSERT INTO world (name, system) VALUES ('Giove', 'Sistema Solare');
INSERT INTO world (name, system) VALUES ('Saturno', 'Sistema Solare');
INSERT INTO world (name, system) VALUES ('Urano', 'Sistema Solare');
INSERT INTO world (name, system) VALUES ('Nettuno', 'Sistema Solare');

INSERT INTO authors(name, surname, nickname) VALUES ('Eichiro', 'Oda', 'Oda sensei');
INSERT INTO books(title, isbn, abstract, publisher, published_date, price, discount) VALUES ('One Piece', '64313', 'Il viaggio del pirata Rufy alla ricerca del one piece', 'Star Comics', '1997-12-24', 4.50, 0);
INSERT INTO book_author(book_id, author_id) VALUES (1, 1);

INSERT INTO users(username, password) VALUES ('admin', '$2a$10$yxZjf/A5ndtHO7zfZ3HsbuG7AZ2RCCMn75f7FoOO1E2x2/BtJFiK6' );