package com.ictcampus.lab.base.control.authentication.model;

import lombok.Value;
import lombok.extern.jackson.Jacksonized;

/**
 * TODO Add Class Description
 */
@Value
@Jacksonized
public class AuthenticationRequest {
	String username;
	String password;
}
