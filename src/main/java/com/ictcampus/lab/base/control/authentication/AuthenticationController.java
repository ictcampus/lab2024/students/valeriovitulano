package com.ictcampus.lab.base.control.authentication;

import com.ictcampus.lab.base.config.jwt.JwtUtil;
import com.ictcampus.lab.base.control.authentication.model.AuthenticationRequest;
import com.ictcampus.lab.base.control.authentication.model.AuthenticationResponse;
import com.ictcampus.lab.base.service.user.UserDetailsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * TODO Add Class Description
 */
@RestController
@RequestMapping("/api/v1/auth")
public class AuthenticationController {
	private final AuthenticationManager authenticationManager;

	private final UserDetailsServiceImpl userDetailsService;

	private final JwtUtil jwtUtil;

	public AuthenticationController(AuthenticationManager authenticationManager, UserDetailsServiceImpl userDetailsService, JwtUtil jwtUtil) {
		this.authenticationManager = authenticationManager;
		this.userDetailsService = userDetailsService;
		this.jwtUtil = jwtUtil;
	}

	@PostMapping("/login")
	public AuthenticationResponse createAuthenticationToken(@RequestBody AuthenticationRequest authenticationRequest) throws Exception {

		try {
			authenticationManager.authenticate(
					new UsernamePasswordAuthenticationToken(authenticationRequest.getUsername(), authenticationRequest.getPassword())
			);
		} catch ( BadCredentialsException e) {
			throw new Exception("Incorrect username or password", e);
		}

		final UserDetails userDetails = userDetailsService
				.loadUserByUsername(authenticationRequest.getUsername());

		final String jwt = jwtUtil.generateToken(userDetails);

		return AuthenticationResponse.builder().jwt( jwt ).build();
	}
}
