package com.ictcampus.lab.base.control.authentication.model;

import lombok.Builder;
import lombok.Value;
import lombok.extern.jackson.Jacksonized;

/**
 * TODO Add Class Description
 */
@Value
@Builder
@Jacksonized
public class AuthenticationResponse {
	String jwt;
}
