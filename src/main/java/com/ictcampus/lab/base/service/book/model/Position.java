package com.ictcampus.lab.base.service.book.model;

import lombok.Data;

/**
 * TODO Add Class Description
 */
@Data
public class Position {
	private Long id;
	private String floor;
	private String sector;
	private String rack;
	private String line;
}
