package com.ictcampus.lab.base.service.book.model;

import lombok.Data;

import java.time.LocalDate;

/**
 * TODO Add Class Description
 */
@Data
public class Author {
	private Long id;
	private String name;
	private String surname;
	private String nickname;
	private LocalDate birthday;
}
