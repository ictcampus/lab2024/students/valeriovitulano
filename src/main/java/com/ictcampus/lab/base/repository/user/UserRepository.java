package com.ictcampus.lab.base.repository.user;

import com.ictcampus.lab.base.repository.user.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * TODO Add Class Description
 */
@Repository
public interface UserRepository extends JpaRepository<UserEntity, Long> {
	UserEntity findByUsername(final String username);

	Boolean existsByUsername(final String username);
}
